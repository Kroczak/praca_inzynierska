import numpy as np
from get_neighbours import get_neighbours
import random 
import matplotlib.colors as colors
import seaborn as sns

class Model:

    def __init__(self, p, q, L, stan_poczatkowy, horyzont):
        self.p = p
        self.q = q
        self.L = L
        if stan_poczatkowy == 'WSZYSCY':
            self.lattice = np.ones((L,L))
        else:
            self.lattice = np.random.randint(2, size=(L,L))
        self.horyzont = horyzont

        pomaranczowy = sns.color_palette('colorblind')[1]
        zielony = sns.color_palette('colorblind')[2]

        cmap = colors.ListedColormap([pomaranczowy,zielony])
        boundaries = [-0.5, 0.5, 1.5]
        self.norm = colors.BoundaryNorm(boundaries, cmap.N, clip=True)   
        self.cmap = cmap

    def krok(self):

        i = np.random.randint(0,self.L)
        j = np.random.randint(0,self.L)

        # jeśli agent ma zachować się niezależnie
        if np.random.rand() < self.p:
            self.lattice[i,j] = np.random.choice([1,0], p = [0.5, 0.5])


        # jeśli agent ma zachować się względem sąsiadów
        else:
            neighbours = get_neighbours(i,j,self.L)

            # usuwanie sąsiadów, których nie potrzebujemy
            while len(neighbours) > self.q:
                neighbours.remove(random.choice(neighbours))

            # muszę sprawdzać True/False na lattice, zależnie od indeksu sąsiadów    
            list_of_lattice = []
            for neighbour in neighbours:
                list_of_lattice.append(self.lattice[neighbour])

            # jeśli są jednomyślni == istnieje tylko jedna opinia
            # może być równe 1??
            if len(set(list_of_lattice)) <= 1:
                self.lattice[i,j] = self.lattice[neighbours[0]]


def get_neighbours(i,j,L):
    
    # zmiana numeru wiersza
    ip = (i+1)%L
    im = (i-1+L)%L
    # zmiana numeru kolumny
    jp = (j+1)%L
    jm = (j-1+L)%L
    
    neighbours = [(im,j), (i,jp), (ip,j), (i,jm)]
    return neighbours
# główny plik do aplikacji

import PySimpleGUI as sg
import pomoc
import okno

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import seaborn as sns
import matplotlib.patches as mpatches
import time

import m2nk
import m2ak
import m3nk
import m3ak


FONT = 'Nunito'

# tworzenie okna
window = okno.window(FONT)

def draw_figure(canvas, figure):
    figure_canvas_agg = FigureCanvasTkAgg(figure, canvas)
    figure_canvas_agg.draw()
    figure_canvas_agg.get_tk_widget().pack(side='top', fill='both', expand=1)
    return figure_canvas_agg

canvas_elem = window['canvas']
canvas = canvas_elem.TKCanvas
fig, ax = plt.subplots(figsize=(6, 6))
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.spines['left'].set_visible(False)
ax.spines['bottom'].set_visible(False)
ax.grid(False)
ax.tick_params(color='white', labelcolor='white')
fig_agg = draw_figure(canvas, fig)

while True:  # Event Loop

    event, values = window.read()


    if event == sg.WIN_CLOSED or event == 'Exit':
        break

    print(values)
    print(values['parametr_p'])

    # Proces menu choices
    if event == 'O aplikacji':
            pomoc.o_aplikacji(FONT)
    if event == 'Instrukcja':      
            pomoc.instrukcja(FONT)
    if event == 'Show':

        if values['-Q2-']:
            q = 2
        else:
            q = 3

        if values['-M10-']:
            L = 10
        elif values['-M20-']:
            L = 20
        else:
            L = 30

        if values['-WSZYSCY-']:
            stan_poczatkowy = 'WSZYSCY'
        else:
            stan_poczatkowy = 'LOSOWY'

        if  values['-dwustanowy-'] and values['-N-']:
            model = m2nk.Model(
                p = values['parametr_p'],
                q = q,
                L = L,
                stan_poczatkowy = stan_poczatkowy,
                horyzont = values['-CZAS-'],
            )

        if  values['-dwustanowy-'] and values['-A-']:
            model = m2ak.Model(
                p = values['parametr_p'],
                q = q,
                L = L,
                stan_poczatkowy = stan_poczatkowy,
                horyzont = values['-CZAS-'],
            )    

        if  values['-trzystanowy-'] and values['-N-']:
            model = m3nk.Model(
                p = values['parametr_p'],
                q = q,
                L = L,
                stan_poczatkowy = stan_poczatkowy,
                horyzont = values['-CZAS-'], 
            )

        if  values['-trzystanowy-'] and values['-A-']:
            model = m3ak.Model(
                p = values['parametr_p'],
                q = q,
                L = L,
                stan_poczatkowy = stan_poczatkowy,
                horyzont = values['-CZAS-'], 
            )

        if values['-trzystanowy-']:
            niebieski = sns.color_palette('colorblind')[0]
            pomaranczowy = sns.color_palette('colorblind')[1]
            zielony = sns.color_palette('colorblind')[2]

            kolory = [zielony,niebieski,pomaranczowy]
            labels = ['Opinia TAK','Opinia NIE WIEM','Opinia NIE']
            patches = [mpatches.Patch(color=kolor, label=label) for kolor, label in zip(kolory, labels)]


        if values['-dwustanowy-']:
            pomaranczowy = sns.color_palette('colorblind')[1]
            zielony = sns.color_palette('colorblind')[2]

            kolory = [zielony,pomaranczowy]
            labels = ['Opinia TAK','Opinia NIE']
            patches = [mpatches.Patch(color=kolor, label=label) for kolor, label in zip(kolory, labels)]

        ax.spines['right'].set_visible(True)
        ax.spines['top'].set_visible(True)
        ax.spines['left'].set_visible(True)
        ax.spines['bottom'].set_visible(True)
        


        while event != 'Pause' and event != 'Exit' and event != sg.WIN_CLOSED:
            event, values = window.read(timeout = 10)
            ax.cla()
            ax.grid(False)
            
            ax.imshow(model.lattice, cmap=model.cmap, norm=model.norm)
            plt.subplots_adjust(top=0.95, bottom=0.2)

            plt.legend(
                handles=patches,
                title = r'$\bf{Możliwe}$ $\bf{opinie}$',
                title_fontsize = 15,
                fontsize = 12,
                loc='upper center',
                bbox_to_anchor=(0.5, -0.05),
                fancybox=True, shadow=True, ncol=3,
                handlelength=1,
                handleheight=1
                )


            model.krok()

            fig_agg.draw()



window.close()
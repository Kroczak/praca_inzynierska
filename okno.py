# funkcja odpowiedzialna za wygląd 

import PySimpleGUI as sg
import os 

PATH = os.getcwd()

kolor = '#343434'

def window(font, theme='LightGreen'):

    sg.theme_background_color(kolor)



    menu_def = [['Pomoc', ['Instrukcja', 'O aplikacji']]]


    # sg.Frame nie ma funkcji 'size'
    # NAJPIERW TWORZĘ POSZCZEGÓLNE ELEMENTY (KAŻDY OSOBNO)
    # POTEM SKŁADAM ELEMENTY W KOLUMNY
    # POTEM NAKŁADAM RAMKI NA KOLUMNY
    # POTEM Z POWYŻSZYCH KLOCKÓW UKŁADAM TŁO


    title = [sg.Text('MODEL Q-WYBORCY NA SIECI KWADRATOWEJ', font=('Nunito', 26), size=(50,1), justification="center", background_color=kolor, text_color='white')]

    # POSZCZEGÓLNE ELEMENTY MUSZĄ BYĆ W LISTACH, ŻEBY MOŻNA JE BYŁO WŁOŻYĆ DO KOLUMN

    ramka_macierz = [sg.Radio('10x10', "RozmiarMacierzy", default=True, size=(10,1), k='-M10-', font=font,  background_color=kolor, text_color='white'),
                    sg.Radio('20x20', "RozmiarMacierzy", default=False, size=(10,1), k='-M20-', font=font,  background_color=kolor, text_color='white'),
                    sg.Radio('30x30', "RozmiarMacierzy", default=False, size=(10,1), k='-M30-', font=font,  background_color=kolor, text_color='white')] 

    ramka_model  = [sg.Radio('model dwustanowy', "model", default=True, size=(15,1), k='-dwustanowy-', font=font,  background_color=kolor, text_color='white'),
                    sg.Radio('model trzystanowy', "model", default=False, size=(15,1), k='-trzystanowy-', font=font,  background_color=kolor, text_color='white')]

    ramka_q  = [sg.Radio('q = 2', "Parametr_q", default=True, size=(15,1), k='-Q2-', font=font,  background_color=kolor, text_color='white'),
                    sg.Radio('q = 3', "Parametr_q", default=False, size=(15,1), k='-Q3-', font=font,  background_color=kolor, text_color='white')]

    ramka_postawa  = [sg.Radio('niezależność', "Postawa", default=True, size=(15,1), k='-N-', font=font,  background_color=kolor, text_color='white'),
                    sg.Radio('antykonformizm', "Postawa", default=False, size=(15,1), k='-A-', font=font,  background_color=kolor, text_color='white')]

    ramka_stan_poczatkowy  = [sg.Radio('tylko opinia TAK', "StanPoczatkowy", default=True, size=(15,1), k='-WSZYSCY-', font=font, background_color=kolor, text_color='white'),
                    sg.Radio('losowe opinie', "StanPoczatkowy", default=False, size=(15,1), k='-LOSOWY-', font=font, background_color=kolor, text_color='white')]
          
    ramka_parametr = [sg.Slider(range=(0,1),
                    default_value=0,
                    resolution=0.01,
                    size=(32,20),
                    orientation='h',
                    font=font,
                    enable_events=True,
                    background_color=kolor,
                    k='parametr_p')]

    ramka_czas = [sg.Slider(range=(1000,10000),
                    default_value=0,
                    resolution=1000,
                    size=(32,20),
                    orientation='h',
                    font=font,
                    enable_events=True,
                    background_color=kolor,
                    k='-CZAS-')]

    start = PATH + '\start.png' 
    stop =  PATH + '\stop.png'  
    exit =  PATH + '\exit.png' 

    ramka_przyciski = [
                    sg.Button(image_filename = start, key='Show', size=(1, 1), border_width=0, button_color=kolor),
                    sg.Button(image_filename = stop, key='Pause', size=(1, 1), border_width=0, button_color=kolor),
                    sg.Button(image_filename = exit, key='Exit', size=(1, 1), border_width=0, button_color=kolor)]
 
    obraz = [sg.Canvas(size=(650, 600), background_color='white', key='canvas')]


    #layout = [[title], [col1, col2]]

    layout = [
        [sg.Menu(menu_def, size=(10,10))], 
        [title],
        
        [
        sg.Column([
        [sg.Frame('   Rozmiar sieci kwadratowej   ', [[sg.Column([ramka_macierz], size=(510,50))]], font=font, background_color=kolor)],
        [sg.Frame('  Liczba możliwych stanów   ' , [[sg.Column([ramka_model], size=(510,50))]], font=font, background_color=kolor)],
        [sg.Frame('  Postawa nonkonformistyczna   ' , [[sg.Column([ramka_postawa], size=(510,50))]], font=font, background_color=kolor)],
        [sg.Frame('   Wartość parametru p   ', [[sg.Column([ramka_parametr], size=(510,60))]], font=font, background_color=kolor)],
        [sg.Frame('   Wartość parametru q   ', [[sg.Column([ramka_q], size=(510,60))]], font=font, background_color=kolor)],
        [sg.Frame('   Stan początkowy układu  ', [[sg.Column([ramka_stan_poczatkowy], size=(510,60))]], font=font, background_color=kolor)],  
        [sg.Frame('  Liczba kroków w symulacji  ' , [[sg.Column([ramka_czas], size=(510,60))]], font=font, background_color=kolor)],
        
        
        ]),

        sg.Column([
        [sg.Frame('   Wizualizacja    ', [[sg.Column([obraz], size=(615,600))]], font=font, background_color=kolor)],

        [sg.Frame('   PRZYCISKI   ',    [[sg.Column([ramka_przyciski], size=(350,60), justification="center", element_justification="center")]], title_color=kolor, font=font, background_color=kolor, element_justification="center", border_width=0)]

        ],  element_justification="center",  justification="center")
        ],
    ]

    dino =  PATH + '\dino2.ico' 

    return sg.Window('Wizualizacja modelu q-wyborcy na sieci kwadratowej', layout, background_color=kolor, finalize=True, icon=dino)


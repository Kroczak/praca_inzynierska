import PySimpleGUI as sg

kolor = '#343434'



def o_aplikacji(font):
    sg.popup(
        'Aplikacja została stworzona na potrzeby zrealizowania pracy inżynierskiej: ,,Modelowanie agentowe dynamiki opinii mierzonych w psychometrycznych skalach Likerta\'\'.\n\nAutor pracy: Marta Kroczak.\nPromotor: prof. dr hab. Katarzyna Weron.\n\nPolitechnika Wrocławska, rok 2022 ',
        title="O aplikacji",
        font=(font,10),
        button_type=5,
        background_color=kolor
    )


def instrukcja(font):
    sg.popup(
        'Aplikacja pozwala na wizualizację różnych wariantów modelu q-wyborcy na sieci kwadratowej. Użytkownik może wybrać następujące własności modelu:\n\n1. Wybór rozmiaru sieci kwadratowej - do wyboru 10x10, 20x20 oraz 30x30.\n\n2. Wybór liczby możliwych stanów - do wyboru model dwustanowy i trzystanowy.\n\n3. Wybór postawy nonkonformistycznej - do wyboru niezależność i antykonformizm.\n\n4. Wybór parametru p - parametr ten odpowiada za prawdopodobieństwo, że w danym kroku agent wykaże się nonkonformistyczną postawą. Parametr należy do [0,1].\n\n5. Wybór parametru q - parametr odpowiada za liczbę sąsiadów, których zdaniem będzie kierował się wylosowany agent w przypadku postawy konformisty lub antykonformisty. Do wyboru jest q = 2 oraz q = 3.\n\n6. Wybór liczby kroków symulacji - wybór czasu trwania symulacji, czyli liczby elementarnych aktualizacji. Najmniejsza liczba kroków to 1000, największa to 10 000.\n\n\nPo wyborze odpowiadających własności należy nacisnąć przycisk START, żeby uruchomić symulację. Symulacja wyświetli się na białym oknie z napisem ,,Wizualizacja\'\'. Naciśnięcie przycisku STOP spowoduje przerwanie działania symulacji. Naciśnięcie przycisku EXIT spowoduje zamknięcie okna aplikacji.',
        title= "Instrukcja",
        font=(font,10),
        button_type=5,
        background_color=kolor
    )
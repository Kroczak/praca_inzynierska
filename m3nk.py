import numpy as np
from get_neighbours import get_neighbours
import random 
import matplotlib.colors as colors
import seaborn as sns


class Model:

    def __init__(self, p, q, L, stan_poczatkowy, horyzont):
        self.p = p
        self.q = q
        self.L = L
        if stan_poczatkowy == 'WSZYSCY':
            self.lattice = np.ones((L,L))*2
        else:
            self.lattice = np.random.randint(3, size=(L,L))
        self.horyzont = horyzont

        niebieski = sns.color_palette('colorblind')[0]
        pomaranczowy = sns.color_palette('colorblind')[1]
        zielony = sns.color_palette('colorblind')[2]

        cmap = colors.ListedColormap([pomaranczowy,niebieski,zielony])
        boundaries = [-0.5, 0.5, 1.5, 2.5]
        self.norm = colors.BoundaryNorm(boundaries, cmap.N, clip=True)   
        self.cmap = cmap

    def krok(self):

        i = np.random.randint(0,self.L)
        j = np.random.randint(0,self.L)


        # jeśli agent ma zachować się niezależnie
        if np.random.rand() < self.p:

            # lista stanów zawiera te stany, do których może przejść agent
            # wybranie każdego z tych stanów ma takie samo prawdopodobieństwo
            # dla agenta = 0 lista wynosi: [0,1], każde z prawdo = 1/2
            # dla agenta = 1 lista wynosi: [0,1,2], każde z prawdo = 1/3
            # dla agenta = 2 lista wynosi: [1,2], każde z prawdo = 1/2

            lista_stanow = list(np.arange(start = max((self.lattice[i,j]-1,0)), stop = min((self.lattice[i,j]+1,2))+1))
            self.lattice[i,j] = np.random.choice(lista_stanow)


        # jeśli agent ma zachować się względem sąsiadów
        else:
            neighbours = get_neighbours(i,j,self.L)

            # usuwanie sąsiadów, których nie potrzebujemy
            while len(neighbours) > self.q:
                neighbours.remove(random.choice(neighbours))

            # muszę sprawdzać wartości na macierzy, zależnie od indeksu sąsiadów    
            list_of_lattice = []
            for neighbour in neighbours:
                list_of_lattice.append(self.lattice[neighbour])

            # jeśli są jednomyślni == istnieje tylko jedna opinia
            if len(set(list_of_lattice)) == 1:

                # jeśli opinia agenta == 1
                if self.lattice[i,j] == 1:
                    # to jego opinia zmienia się na opinię sąsiadów 
                    self.lattice[i,j] = self.lattice[neighbours[0]]

                # jeśli opinia agenta == 0 lub opinia agenta == 2
                else:
                    # jeśli opinia agenta == opinii sąsiadów, to nic się nie dzieje

                    # jeśli opinia agenta nie równa się opinii sąsiadów
                    # to musi nastąpić zmiana ze stanu skrajnego na stan neutralny
                    if self.lattice[i,j] != self.lattice[neighbours[0]]:
                        self.lattice[i,j] = 1

